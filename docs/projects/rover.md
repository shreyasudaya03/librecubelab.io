# Rover Project

## Overview

The objective is to develop an autonomous rover that can be easily adapted for various
applications. For example, one could think of using the rover to support disaster relief as it
can drive around and allow to making distress calls. Or it could be used as mobile guard to
monitor locations. Or a bunch of those rovers can be used to demonstrate formation driving
giving insight to support Moon exploration missions. Or just it as a fun toy to drive around
in your garden.

No matter the application, the rover will be designed using space communication protocols and
space engineering best practices.

The baseline scenario is depictured below. 

![](assets/rover_scenario.png)

The scenario objectives are:

- Receive period telemetry (TM) from rover (such as temperatures, voltage levels, on/off status, position).
- Send telecommand (TC) to the rover to capture an image with the camera.
- Download the picture via CCSDS File Delivery Protocol (CFDP).
- Send telecommands to the rover to control its driving.
- Uplink an On-Board Control Procedures (OBCP) that contains the commands to control the rover drive.
- Send TC to run the OBCP (and thus to control rover drive).

The rover will consist of mechanical parts, electronic boards, and software applications.
Inside the rover, a stack of electronic boards will implement most of the functionality.

![](assets/rover_board_stack.png)

The boards are interconnected through the common CubeSat 104-pin connector using the [LibreCube Board Specification](https://librecube.gitlab.io/standards/board_specification/).

Below is a block diagram of the various components and boards and their interfacing.

![](assets/rover_block_diagram.png)

The abbreviations are as follows:

- OBC: On-Board Computer
- PCDU: Power Distribution and Control Unit
- COM: Communications Unit

There are three on-board busses to be used:

- **System bus**: The OBC controls and monitors the other boards. It uses the [SpaceCAN protocol](https://librecube.gitlab.io/standards/spacecan/). The goal of this bus is not to transfer big amounts of data, but to provide a reliable way for OBC to send commands to, and obtain telemetry from, other boards.

- **Payload bus**: The OBC interfaces with the camera via the payload bus. This bus is not used to control/monitor the camera but to transfer image data. OBC commands (via system bus) the transfer of image data, which would then be sent over this payload bus. As this bus is not so critical like the system bus, (limited) data loss can be accepted (ie. images can be re-requested if incomplete).

- **Comms bus**: The COM board is used to send and receive data over the radio link to external system (mostly ground station). For this it modulates a bit stream (containing outgoing data, like telemetry) into RF signal. Likewise, it decodes a bit stream from incoming RF signal (which contains mostly commands from ground). The coms bus therefore transports bit streams between OBC and COM board.

The interface between ground station (GS) and rover shall be implemented as [a protocol stack](https://librecube.gitlab.io/reference_architecture/#remote-segment-ground-station-system). For demonstration purpose we will use UDP over WiFi instead of radio modulation, but will add the proper physical layer at later stage.

The interface between ground station and mission operations center (MOC) shall be based on [Space Link Extension](https://librecube.gitlab.io/reference_architecture/#ground-station-system-mission-operations-system) (SLE) protocol, which itself encapsulates forward and return CCSDS frames and rides on internet protocol stack (TCP/IP).

## Development Setup

Shown below is a recommended setup to support the rover electronics stack development. The pyboards were chosen in particular as they provide two integrated CAN modules, as needed for the redundant CAN bus. They need to be accomplemented with CAN transceiver skins.

![](assets/rover_development_setup.png)

The suggested components for the development setup are:

- 4x pyboards
- 4x [CAN transceiver skins](https://gitlab.com/librecube/tools/pyboard-can-transceiver-skin)
- 1x OpenMV H7 R1
- 1x [LC2201](https://gitlab.com/librecube/elements/LC2201) (or a 5V power supply)
- 1x Linx PCB-EVM-RM
- 1x Pycom WiPy
- 1x PCA9685 PWM module
- 4x Parallax Standard Servo
- 4x Parallax Continuous Rotation Servo
- 1x Raspberry Pi
- 1x RS485 CAN HAT for Raspberry Pi

## Work Packages Overview

All of the above work is subdivided into smaller work packages (WP). Most of them
are fairly decoupled from each other, so you can pick your WP of choice and work
on it (alone or with others), and by doing so, push the entire rover project a step
further towards completion.

The progress and allocation of work packages is [organized in the Kanban](https://tree.taiga.io/project/librecube-librecube/epic/295).

If you are not sure where to start, it is always a good idea to post to the [main chat channel of LibreCube](https://app.element.io/#/room/#librecube.org:matrix.org).

## Work Packages - Electronics

### WP E1: Wheel Drive Board

- Repository: [https://gitlab.com/librecube/prototypes/platform-wheel-drive-board](https://gitlab.com/librecube/prototypes/platform-wheel-drive-board)
- Kanban: [https://tree.taiga.io/project/librecube-librecube/us/301](https://tree.taiga.io/project/librecube-librecube/us/301)

This board controls the movement (direction and speed) of a set of attached wheels. It is interfaced via SpaceCAN and shall support commands like “drive forward with speed 50% for 5 seconds”, etc. It uses a microcontroller that support CAN and PWM (eg pyboard). It shall control up to 6 wheels, each with a servo and rotation motor.

### WP E2: On-Board Computer Board

- Repository: None yet
- Kanban: None yet

This is the main processing unit of the rover.
It will run all the heavy on-board software: CFDP, OBCP engine, CCSDS protocol stack and ECSS PUS protocol. The above applications may be implemented as docker containers and
are developed in the frame of software work packages (see further below). At later stage, the OBC firmware design shall be oriented on the SAVOIR functional reference architecture.

### WP E3: Communications Board

- Repository: None yet
- Kanban: None yet

To be written...

### WP E4: Attitude and Position Board

- Repository: None yet
- Kanban: None yet

To be written...

### WP E5: Camera Board

- Repository: None yet
- Kanban: None yet

To be written...

## Work Packages - Mechanical

### WP M1: Wheel Assembly

- Repository: [https://gitlab.com/librecube/prototypes/platform-wheel-assembly](https://gitlab.com/librecube/prototypes/platform-wheel-assembly)
- Kanban: [https://tree.taiga.io/project/librecube-librecube/us/285](https://tree.taiga.io/project/librecube-librecube/us/285)

Develop a wheel assembly that can be mount on a 1U Structure (4 wheels) and a 2U Structure (4 and 6 wheels). The mounting shall be done on the frame windows (which have standard size and mounting holes).

### WP M2: Solar Array Assembly

- Repository: [https://gitlab.com/librecube/prototypes/platform-solar-panels](https://gitlab.com/librecube/prototypes/platform-solar-panels)
- Kanban: [https://tree.taiga.io/project/librecube-librecube/us/306](https://tree.taiga.io/project/librecube-librecube/us/306)

To be written...

## Work Packages - Software

### WP S1: System Bus (SpaceCAN)

- Repository: None yet
- Kanban: None yet

To be written...

### WP S2: Payload Bus

- Repository: None yet
- Kanban: None yet

To be written...

### WP S3: Communications Bus

- Repository: None yet
- Kanban: None yet

To be written...

### WP S4: On-Board Control Procedure (OBCP) Engine

- Repository: [https://gitlab.com/librecube/prototypes/python-pluto-executor](https://gitlab.com/librecube/prototypes/python-pluto-executor)
- Kanban: [https://tree.taiga.io/project/librecube-librecube/us/14](https://tree.taiga.io/project/librecube-librecube/us/14)

The OBCP engine executes PLUTO scripts (ie. procedures) on board the rover.
It shall be executed by the processor (Raspberry Pi) of the OBC board.
The OBCP engine shall be controlled via external commands (ie. from mission control)
to load/start/stop/unload PLUTO scripts.

When running a procedure, it will emit commands to the other parts of the rover
system (eg. control the wheels, take pictures, etc.) and shall have access to
the status of the rover.

The implementation shall be in accordance with ECSS-E-ST-70-01C.

### WP S5: CFDP Engine

- Repository: [https://gitlab.com/librecube/lib/python-cfdp](https://gitlab.com/librecube/lib/python-cfdp)
- Kanban: [https://tree.taiga.io/project/librecube-librecube/us/310](https://tree.taiga.io/project/librecube-librecube/us/310)

CFDP is the CCSDS File Delivery Protocol. It is the backbone protocol that enables
file-based operation, which is seen as the future of satellite operations. In short,
at allows to reliably send and receive files between satellite and ground.

### WP S6: Space Data Link Protocol

- Repository: [https://gitlab.com/librecube/prototypes/python-sdlp](https://gitlab.com/librecube/prototypes/python-sdlp)
- Kanban: [https://tree.taiga.io/project/librecube-librecube/us/308](https://tree.taiga.io/project/librecube-librecube/us/308)

To be written...

### WP S7: Space Packet Protocol

- Repository: [https://gitlab.com/librecube/lib/python-spp](https://gitlab.com/librecube/lib/python-spp)
- Kanban: [https://tree.taiga.io/project/librecube-librecube/us/307](https://tree.taiga.io/project/librecube-librecube/us/307)

To be written...

### WP S8: Packet Utilization Service (PUS) Protocol

- Repository: None yet
- Kanban: None yet

To be written...
