# Open Source

At LibreCube we are convinced that the philosophy of open source is urgently
needed to democratize space exploration!

Why? Because open source is:

- **Freedom**: You are free to use and adapt the software as you need.
- **Reliable**: It is the backbone of the internet as we know it and
powers much of the world's IT infrastructure.
- **Connecting People**: It allows us to work together, sharing knowledge and
resources.  
- **Fun**: Try it out!

We aim to use and produce software that is cross-platform and runs on most
common operations systems, but the preference is Linux.

## Open Source Software Tools

Listed here are open source tools we commonly use and recommend as baseline.

**Office and Collaboration**

| Name | Description
|-|-
| [LibreOffice](https://www.libreoffice.org/) | The LibreOffice suite comprises programs for word processing, the creation and editing of spreadsheets, slideshows, diagrams and drawings, working with databases, and composing mathematical formulae.
| [DBeaver](https://dbeaver.jkiss.org/) | DBeaver is an SQL client and a database administration tool. It provides an editor that supports code completion and syntax highlighting. This is a desktop application written in Java and based on Eclipse platform.
| [Taiga](https://www.taiga.io/) | Taiga is a agile project management web application for software developments and other projects. It provides Kanban or Scrum methodology. Backlogs are shown as a running list of all features and User Stories added to the project.


**Engineering**

| Name | Description
|-|-
| [FreeCAD](http://www.freecadweb.org/) | FreeCAD is a general-purpose parametric 3D CAD modeler and a building information modeling software with finite-element-method support. FreeCAD is aimed directly at mechanical engineering product design. FreeCAD can be used interactively, or its functionality can be accessed and extended using the Python programming language.
| [OpenSCAD](http://www.openscad.org/) | OpenSCAD is a free software application for creating solid 3D CAD objects. It is a script-only based modeller that uses its own description language; parts can be previewed, but it cannot be interactively selected or modified by mouse in the 3D view. An OpenSCAD script specifies geometric primitives (such as spheres, boxes, cylinders, etc.) and defines how they are modified and combined (for instance by intersection, difference, envelope combination and Minkowski sums) to render a 3D model.
| [Blender](http://www.blender.org/) | Blender is a professional 3D computer graphics software toolset used for creating animated films, visual effects, art, 3D printed models, interactive 3D applications and video games.
| [KiCad](http://www.kicad-pcb.org/) | KiCad is a software suite for electronic design automation (EDA). It facilitates the design of schematics for electronic circuits and their conversion to PCB designs. It features an integrated environment for schematic capture and PCB layout design. Tools exist within the package to create a bill of materials, artwork, Gerber files, and 3D views of the PCB and its components.
| [Papyrus](https://eclipse.org/papyrus/) | Papyrus is an UML tool based on Eclipse. It can either be used as a standalone tool or as an Eclipse plugin. It provides support for Domain Specific Languages and SysML. Papyrus is designed to be easily extensible as it is based on the principle of UML Profiles.  

**Software Development**

| Name | Description
|-|-
| [VSCodium](https://vscodium.com/) | VSCodium is a community-driven, freely-licensed binary distribution of Microsoft’s editor VS Code.

## Open Source Licenses

To publish your works as open source, you must include an open source license
in your project repository to let others know under what exact conditions they
can make use of your works.

You are free to decide which license to use (but stick to [OSI approved licenses](https://opensource.org/licenses)!)

Here is a guideline to help choosing an appropriate license:

- **The MIT License**: We use this one for software libraries, to allow others
to modify them to their needs. It is a very permissive license.

- **GNU General Public License version 3**: We use this mostly for software
applications. This way, anyone can run/modify/distribute the software, but they
are not allowed to integrate it in closed-source projects. Derived works must
remain open source.

- **CERN Open Hardware Licence Version 2**: We use this one for hardware projects.
